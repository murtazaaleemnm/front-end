import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import Classes from "../../tables/Classes";
import "react-tabs/style/react-tabs.css";
//design import
import { withTranslation } from "react-i18next";
class ClassTabs extends Component {
  render() {
    let { t } = this.props;
    return (
      <Tabs>
        <TabList>
          <Tab>{t("tabs-titles.class-list")}</Tab>
        </TabList>

        <TabPanel>
          <Classes {...this.props} />
        </TabPanel>
      </Tabs>
    );
  }
}

export default withTranslation()(ClassTabs);
