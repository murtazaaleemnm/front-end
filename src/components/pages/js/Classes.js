import React, { Component } from "react";
import Tabs from "../../tabs/classes";
import CreateClassForm from "../../forms/classes/CreateClassForm";

import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import "../css/Classes.css";
//imports for lessons api call
import { connect } from "react-redux";
import {
  fetchClasses,
  deleteClass,
  fetchTeachers,
  fetchRooms,
  fetchLessons,
} from "../../../actions";
import { withTranslation } from "react-i18next";
class Classes extends Component {
  componentDidMount() {
    this.props.fetchClasses();
    this.props.fetchTeachers();
    this.props.fetchRooms();
    this.props.fetchLessons();
  }
  render() {
    return (
      <div>
        <Grid container spacing={3}>
          <Grid item xs={9}>
            <Tabs {...this.props} />
          </Grid>
          <Grid item xs={3}>
            <Paper>
              <CreateClassForm {...this.props} />
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    classess: Object.values(state.classs),
    teachers: Object.values(state.teacher),
    rooms: Object.values(state.room),
    lessons: Object.values(state.lesson),
  };
};

const wrapped = connect(mapStateToProps, {
  fetchClasses,
  deleteClass,
  fetchTeachers,
  fetchRooms,
  fetchLessons,
})(Classes);

export default withTranslation()(wrapped);
