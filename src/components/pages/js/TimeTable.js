import React, { Component } from "react";
import Time from "../../tables/Time";
import "../css/TimeTable.css";

//imports for teachers api call
import { connect } from "react-redux";
import {
  fetchTime,
  createTime,
  deleteTime,
  fetchClasses,
  fetchLessons,
} from "../../../actions";
import { withTranslation } from "react-i18next";
class TimeTable extends Component {
  componentDidMount() {
    this.props.fetchTime();
    this.props.fetchClasses();
    this.props.fetchLessons();
  }
  handleClick() {
    let { createTime, fetchTime } = this.props;
    createTime().then(() => fetchTime());
  }
  render() {
    return (
      <>
        <Time {...this.props} />
        <div className='float' onClick={() => this.handleClick()}>
          <div className='my-float' style={{ fontSize: "35px" }}>
            +
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    time: Object.values(state.time),
    classes: Object.values(state.classs),
    lessons: Object.values(state.lesson),
  };
};

const wrapped = connect(mapStateToProps, {
  fetchTime,
  createTime,
  deleteTime,
  fetchClasses,
  fetchLessons,
})(TimeTable);

export default withTranslation()(wrapped);
