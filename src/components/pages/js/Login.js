import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from "react-router-dom";

//imports for users api call
import { connect } from "react-redux";
import { loginUser } from "../../../actions";

class Login extends Component {
  renderError({ error, touched }) {
    if (touched && error) {
      return (
        <div className='ui error message'>
          <div className='header'> {error}</div>
        </div>
      );
    }
  }
  renderInput = ({ input, label, meta, type, placeholder }) => {
    const className = `field ${meta.error && meta.touched ? "error" : ""}`;
    return (
      <div className={className}>
        <label>{label}</label>
        <input
          {...input}
          autoComplete='off'
          type={type}
          placeholder={placeholder}
        />
        {this.renderError(meta)}
      </div>
    );
  };

  onSubmit = (formValues) => {
    console.log(formValues);
    this.props.loginUser(formValues);
  };
  render() {
    console.log(this.props);
    return (
      <div
        className='ui middle aligned center aligned grid'
        style={{ height: "-webkit-fill-available" }}
      >
        <div className='five wide column'>
          <h2 className='ui blue header'>
            <div className='content'>Log-in to your account</div>
          </h2>
          <form
            onSubmit={this.props.handleSubmit(this.onSubmit)}
            className='ui large form error'
          >
            <Field
              name='username'
              component={this.renderInput}
              label='Enter username'
              type='text'
              placeholder='E-mail address'
            />
            <Field
              name='password'
              component={this.renderInput}
              label='Enter password'
              type='password'
              placeholder='Password'
            />
            <button className='ui fluid large blue submit button'>Login</button>
          </form>

          <div className='ui message'>
            New to us? <Link to='/signup'>Sign Up</Link>
          </div>
        </div>
      </div>
    );
  }
}

const validate = (formValues) => {
  const errors = {};

  if (!formValues.username) {
    //only ran if user not entered username
    errors.username = "you must enter username ";
  }

  if (!formValues.password) {
    //only ran if user not entered password
    errors.password = "you must enter password ";
  }
  return errors;
};

const formWrapped = reduxForm({
  form: "login",
  validate,
})(Login);

export default connect(null, { loginUser })(formWrapped);
