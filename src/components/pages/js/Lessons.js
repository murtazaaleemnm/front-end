import React, { Component } from "react";
import Tabs from "../../tabs/lessons";
import CreateLessonForm from "../../forms/lessons/CreateLessonForm";

import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/styles";
import "../css/Lessons.css";
//imports for lessons api call
import { connect } from "react-redux";
import { fetchLessons, deleteLesson } from "../../../actions";
import { withTranslation } from "react-i18next";
class Lessons extends Component {
  componentDidMount() {
    this.props.fetchLessons();
  }
  render() {
    return (
      <div>
        <Grid container spacing={3}>
          <Grid item xs={9}>
            <Tabs {...this.props} />
          </Grid>
          <Grid item xs={3}>
            <Paper>
              <CreateLessonForm {...this.props}/>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { lessons: Object.values(state.lesson) };
};

const wrapped = connect(mapStateToProps, { fetchLessons, deleteLesson })(
  Lessons
);

export default withTranslation()(wrapped);
