import React, { Component } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

class SimpleTable extends Component {
  render() {
    const { lessons, deleteLesson, t } = this.props;
    console.log("{Lessons table}", lessons);
    return (
      <TableContainer component={Paper}>
        <Table aria-label='simple table'>
          <TableHead>
            <TableRow>
              <TableCell>
                {t("table-fields.lessons-table.lesson-code")}
              </TableCell>
              <TableCell>
                {t("table-fields.lessons-table.lesson-name")}
              </TableCell>
              <TableCell>{t("table-fields.delete")}</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {lessons && lessons[0]
              ? lessons[0].map((lesson, index) => (
                  <TableRow key={index}>
                    <TableCell>{lesson.code}</TableCell>
                    <TableCell>{lesson.name}</TableCell>
                    <TableCell>
                      <Button
                        variant='contained'
                        color='secondary'
                        onClick={() => deleteLesson(lesson._id)}
                      >
                        {t("Button.delete")}
                      </Button>
                    </TableCell>
                  </TableRow>
                ))
              : null}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

export default SimpleTable;
