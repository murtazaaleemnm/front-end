import React, { Component } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

class SimpleTable extends Component {
  renderLessons(lessons) {
    return (
      <>
        {lessons.map((lesson) => {
          return <div>{lesson.label}</div>;
        })}
      </>
    );
  }
  render() {
    let { teachers, deleteTeacher, t } = this.props;
    console.log("[table teachers]", teachers);
    return (
      <>
        <TableContainer component={Paper}>
          <Table aria-label='simple table'>
            <TableHead>
              <TableRow>
                <TableCell>
                  {t("table-fields.teachers-table.teacher-name")}
                </TableCell>
                {/* <TableCell align='right'>
                  {t("table-fields.teachers-table.teacher-age")}
                </TableCell> */}
                <TableCell align='right'>
                  {t("table-fields.teachers-table.teacher-code")}
                </TableCell>
                <TableCell align='right'>
                  {t("table-fields.teachers-table.teacher-lvl")}
                </TableCell>
                <TableCell align='right'>
                  {t("table-fields.teachers-table.teacher-lessons")}
                </TableCell>
                <TableCell align='right'>{t("table-fields.delete")}</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {teachers && teachers[0]
                ? teachers[0].map((teacher) => (
                    <TableRow key={teacher.code}>
                      <TableCell component='th' scope='row'>
                        {teacher.name}
                      </TableCell>
                      <TableCell align='right'>{teacher.age}</TableCell>
                      <TableCell align='right'>{teacher.code}</TableCell>
                      <TableCell align='right'>{teacher.grade}</TableCell>
                      <TableCell align='right'>
                        {this.renderLessons(teacher.lessons)}
                      </TableCell>
                      <TableCell align='right'>
                        <Button
                          variant='contained'
                          color='secondary'
                          onClick={() => deleteTeacher(teacher._id)}
                        >
                          {t("Button.delete")}
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))
                : null}
            </TableBody>
          </Table>
        </TableContainer>
      </>
    );
  }
}

export default SimpleTable;
