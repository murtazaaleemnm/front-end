import React, { Component } from "react";

//imports for users api call
import { connect } from "react-redux";
import { createLesson } from "../../../actions";

class CreateLesson extends Component {
  constructor(props) {
    super(props);
    this.state = { code: "", name: "" };

    this.handleChangeCode = this.handleChangeCode.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeName(event) {
    this.setState({ name: event.target.value });
  }
  handleChangeCode(event) {
    this.setState({ code: event.target.value });
  }
  handleSubmit(event) {
    alert("Шинэ хичээл үүслээ");
    let values = {
      code: this.state.code,
      name: this.state.name,
    };
    this.props.createLesson(values);
  }
  render() {
    let { t } = this.props;
    return (
      <form className='form' onSubmit={this.handleSubmit}>
        <div className=' form-group'>
          <label>{t("form.label.lesson-code")}:</label>
          <input
            type='text'
            className='form-control'
            value={this.state.code}
            onChange={this.handleChangeCode}
            placeholder={t("form.placeholder.lesson-code")}
          />
        </div>
        <div className=' form-group'>
          <label>{t("form.label.lesson-name")}:</label>
          <input
            type='text'
            className='form-control'
            value={this.state.name}
            onChange={this.handleChangeName}
            placeholder={t("form.placeholder.lesson-name")}
          />
        </div>
        <div className='d-flex button' style={{ justifyContent: "center" }}>
          <button type='submit' class='btn btn-outline-primary'>
            {t("Button.create")}
          </button>
        </div>
      </form>
    );
  }
}

export default connect(null, { createLesson })(CreateLesson);
