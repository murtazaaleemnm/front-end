import _ from "lodash";
import { CREATE_ROOM, FETCH_ROOMS, DELETE_ROOM } from "../actions/types";

export default (state = {}, action) => {
  switch (action.type) {
    case CREATE_ROOM:
      return { ...state, [action.payload.id]: action.payload };
    case FETCH_ROOMS:
      return { ...state, [action.payload.id]: action.payload };
    case DELETE_ROOM:
      //omit n shine state uusgej bga ba action payload deer irsen id tai hereglegch ustah um
      return _.omit(state, action.payload);
    default:
      return state;
  }
};
