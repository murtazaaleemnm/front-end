import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import authReducer from "./authReducer";
import userReducer from "./userReducer";
import roomReducer from "./roomReducer";
import teacherReducer from "./teacherReducer";
import lessonReducer from "./lessonReducer";
import classReducer from "./classReducer";
import timeReducer from './timeReducer';

//Combines into one store
export default combineReducers({
  auth: authReducer,
  form: formReducer,
  user: userReducer,
  room: roomReducer,
  teacher: teacherReducer,
  lesson: lessonReducer,
  classs: classReducer,
  time: timeReducer
});
