import _ from "lodash";
import { CREATE_TIME, FETCH_TIME, DELETE_TIME } from "../actions/types";

//CRUD time
// manages time list
export default (state = {}, action) => {
  switch (action.type) {
    case CREATE_TIME:
      return { ...state, [action.payload.id]: action.payload };
    case FETCH_TIME:
      return { ...state, [action.payload.id]: action.payload };
    case DELETE_TIME:
      //omit n shine state uusgej bga ba action payload deer irsen id tai hereglegch ustah um
      return _.omit(state, action.payload);
    default:
      return state;
  }
};
